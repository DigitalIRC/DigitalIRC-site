---
title: Staff
author: MrRandom
layout: page
Views:
  - 2
---
List of current Network Staff

&nbsp;

<table>
  <tr class="row-1">
    <th class="column-1" width="33%">
      Nickname
    </th>
    
    <th class="column-2" width="33%">
      Hostmask
    </th>
    
    <th class="column-3" width="34%">
      Position
    </th>
  </tr>
  
  <tr class="row-2">
    <td class="column-1">
      MrRandom
    </td>
    
    <td class="column-2">
      me@MrRandom.my.explode.on.impact
    </td>
    
    <td class="column-3">
      Network Admin
    </td>
  </tr>
  
  <tr class="row-3">
    <td class="column-1">
      Barumba
    </td>
    
    <td class="column-2">
      barumba@ladyboys.rule
    </td>
    
    <td class="column-3">
      Services Admin
    </td>
  </tr>
  
  <tr class="row-4">
    <td class="column-1">
      Clownius
    </td>
    
    <td class="column-2">
      Empornium@clown.empornium.me
    </td>
    
    <td class="column-3">
      Server Admin
    </td>
  </tr>
  
  <tr class="row-5">
    <td class="column-1">
      DippingSauce
    </td>
    
    <td class="column-2">
      dippingsa@no.double.dipping
    </td>
    
    <td class="column-3">
      Server Admin
    </td>
  </tr>
  
  <tr class="row-6">
    <td class="column-1">
      ^tammy^
    </td>
    
    <td class="column-2">
      tammy@purveyor.of.fine.bitches.since.2004
    </td>
    
    <td class="column-3">
      Server Admin
    </td>
  </tr>
  
  <tr class="row-7">
    <td class="column-1">
      yamchan
    </td>
    
    <td class="column-2">
      yamchan0@yamchan.opers.digitalirc.org
    </td>
    
    <td class="column-3">
      Global Operator
    </td>
  </tr>
  
  <tr class="row-8">
    <td class="column-1">
      Mobbo
    </td>
    
    <td class="column-2">
      mobbo@mobbo.opers.digitalirc.org
    </td>
    
    <td class="column-3">
      Global Operator
    </td>
  </tr>
  
  <tr class="row-9">
    <td class="column-1">
      b0rk
    </td>
    
    <td class="column-2">
      borkb0rk@b0rk.users.digitalirc.org
    </td>
    
    <td class="column-3">
      Global Operator
    </td>
  </tr>
</table>

&nbsp;

Classes:

*   Local Oper&#8217;s can only affect users on their server&#8217;s
*   Global Oper&#8217;s can affect any user on the network
*   Server Admin&#8217;s can global ban IP&#8217;s & edit the ircd config
*   Network Admin&#8217;s can edit the ircd config and have root access to services.