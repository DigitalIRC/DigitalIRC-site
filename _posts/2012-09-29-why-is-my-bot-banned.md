---
title: Why is my bot banned
author: MrRandom
layout: post
---
So your bot can&#8217;t connect and it&#8217;s saying its been banned from the network. This has happened because your bot was either:

1.  Spamming users, opers or channels
2.  In a channel where the channel owner hadn&#8217;t given it permission

If your bot didn&#8217;t run ident then your entire host has been banned from the network. If you bot did respond to ident then only it was banned so you can still connect to the server just not with the account the bot was on.

To appeal the ban contact an operator in #help or email opers (@) digitalirc (dot) org
