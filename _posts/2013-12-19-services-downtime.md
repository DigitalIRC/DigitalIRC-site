---
title: Services Downtime
author: MrRandom
layout: post
---

We are planning a short maintenance window for the 20th December 2013 at 12:00 GMT and should only last 1 hour. This will allow us to upgrade our services and carry out minor database management.

During the downtime chanserv and nickserv authentication and channel management will be unavailable. Services will be put into read-only mode 30 minutes before the maintenance is scheduled to start.
StatServ and the RSS bots should not be disrupted but the services web interface will also be unavailable.

### Update

The update has been completed fully and services are now all running at 100%