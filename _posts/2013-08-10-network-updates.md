---
title: Network updates!
author: MrRandom
layout: post
---
On Monday 12 August 2013 we will be migrating the network from UnrealIRCd to InspIRCd.

This will allow us to have more fine grained control over the features the network offers.

The downtime is planned to start at 16:00 GMT until 18:00 GMT

~~This post will be updated when the update has happened.~~ Update completed successfully.
