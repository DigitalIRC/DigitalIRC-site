---
title: Welcome terra to the network
author: MrRandom
layout: post
---

Please welcome terra.digitalirc.org to the network admin&#8217;ed by me (MrRandom). It&#8217;s based in Denver, Colorado and it supports standard SSL (port 6697) and IPv6.

-MrR
