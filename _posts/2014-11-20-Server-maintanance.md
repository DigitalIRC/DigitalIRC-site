---
title: Mooo offline for maintenance
author: MrRandom
layout: post
except: Temporary server downtime
---
Mooo and out some of our extended services will be going offline for maintenance on Friday 28th November. This downtime is to facilitate server side updates and software updates that would normally disrupt the IRC network.

During this time the other servers & core services such as the webchat and nickserv. will remain online. Mooo will be taken out of the server pool on the 27th November to allow for DNS to update fully.

- MrRandom