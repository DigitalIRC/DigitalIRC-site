---
title: Can I get a custom BotServ bot?
author: MrRandom
layout: post
---
Yes, you can! There are 2 classifications of botserv bots, Public and Private. These are detailed below.

### Public Bots

Anyone may request a public bot absolutely free. These bots are available for all users to assign to their channels. However, they must be completely generic and not channel specific – for example a <a href="http://www.bbc.co.uk/programmes/b006mf4b" target="_blank">Spooks</a> themed bot called HarryPearce is acceptable, whereas a bot named GamesBot for #games would not be acceptable.

You may request a bot be added in #help on Digital. When you ask, you must state a nickname, ident, hostname and Gecos – e.g. “HarryPearce harry@runs.MI5 Harry Pearce from Spooks”. Bots must be creative and worth adding. If a staff member deems your suggestion is a good one, your bot will be added and an unused bot will be deleted.

This whole process should help us provide a better range of bots to our users.

### Private Bots

Private bots can have any credentials that you like and will not be available to all users. When you request a private bot, you must state a nickname, ident, hostname, Gecos and channels to which the bot should be assigned. After initial creation, you may request a single change to your BotServ bot every 30 days. A single change is counted as a change to any of the bot’s credentials or the set of channels in which it resides.

If you would like a public or private bot, come and see us in [#help][1]

 [1]: http://www.digitalirc.org/webchat-help/ "Help"
