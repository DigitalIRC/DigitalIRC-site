---
title: Using Mibbit
author: MrRandom
layout: post
---
Digital IRC has registered with mibbit, what this means is that unlike some other networks when you connect via mibbit your IP/hostname doesn&#8217;t appear as your real name.

&nbsp;

To use this feature specify the server irc.digitalirc.org when connecting. This tells mibbit your connecting to our network and to auth with our IRC servers so that your IP can be hidden.

&nbsp;

Other addesses you can use to connect with are:

*   exodus.digitalirc.org
*   mooo.digitalirc.org
*   shire.digitalirc.org
*   irc.eu.digitalirc.org
*   irc.us.digitalirc.org

Please note that ipv6.digitalirc.org DOES NOT work with mibbit.
