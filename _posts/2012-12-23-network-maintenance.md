---
title: Network Maintenance
author: MrRandom
layout: post
---
Since the network is quieting down for the holiday period we will be doing some maintenance and testing on various parts of the network.

This should start at roughly 0000 hours (GMT) this Friday (27 December 2012)

<del>The maintenance should only last a few hours but is planned to last the entire day. This post will be updated when it is over.</del>

The network has been updated and all services should be running normally.
