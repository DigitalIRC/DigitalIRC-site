---
title: Alternate Webchat
author: MrRandom
layout: post
---
For those that want to run their own webchat without having to rely on our main <a title="webchat" href="http://webchat.digitalirc.org/" target="_blank">webchat</a> we have now provided support for <a href="http://www.lightirc.com/" target="_blank">LightIRC</a>.

LightIRC is a Flash IRC client that includes all well known IRC features. It supports style sheets for its own skins, and has support for multiple languages. This allows greater customization of the client as per your needs.

To use lightIRC on DigitalIRC Network use following settings in the config.js in the lightIRC download.

> params.host = &#8220;shire.digitalirc.org&#8221;;  
> params.port = 6667;  
> params.policyPort = 16163;

A full sample config can be [found here][1].

If you wish to use our lightIRC please <a href="/lightirc/" target="_blank">click here.</a>

-MrR

 [1]: /wp-content/uploads/2012/12/config.js
