---
layout: default
title: Status Page
---
<?php
//include stuff
$config = include('include/config.php');
include('include/cache.php');

// Create cache
$cache = new SimpleCache();
$cache->cache_path = 'cache/';
$cache->cache_time = 300;
$label = 'xml';
$url = "https://api.uptimerobot.com/getMonitors?apiKey=" . $config[apiKey] . "&customUptimeRatio=30&format=xml";
$error = TRUE;

// check we're caching correctly (Dev only). Should remove this at some point
if($cache->is_cached($label)){
    echo "<!--Data IC-->";
}else{
    echo "<!--Data NIC-->";
}

// Get the data & parse it
if (($xml = $cache->get_data($label, $url))===false) {
    echo "<!-- Error -->";
    $error = TRUE;
} else {
    $error = FALSE;
    $xml = new SimpleXMLElement ($xml);
}

echo "\n<h3>Updated every 5 minutes</h3>\n" . 
     "<table class=\"table table-striped\">\n".
     "    <thead>\n".
     "        <th>Name</th><th>Status</th><th>Monthly Uptime</th><th>Type</th>\n".
     "    </thead>\n".
     "    <tbody>\n";
if ($error == FALSE) {
    foreach($xml->monitor as $monitor) {
        echo "        <tr>\n".
             "            <td>";
        if (strpos($monitor['url'], 'http') === 0) {
            echo "<a href=" . $monitor['url'] . ">" . $monitor['friendlyname'] . "</a>";
        } else {
            echo $monitor['friendlyname'];
        }
        echo "</td><td>";
        if ($monitor['status'] == 2) {
            echo "<span class=\"label label-success\">Online</span>";
        }
        elseif ($monitor['status'] == 8) {
            echo "<span class=\"label label-danger\">Offline</span>";
        }
        elseif ($monitor['status'] == 9) {
            echo "<span class=\"label label-danger\">Offline</span>";
        }
        else {
            echo "<span class=\"label label-primary\">Unknown</span>";
        }
        echo "</td><td>";
        if ($monitor['alltimeuptimeratio'] > 97) {
            echo "<div class=\"progress\">".
                 "<div class=\"progress-bar progress-bar-success\" role=\"progressbar\" aria-valuenow=\"" . $monitor['customuptimeratio'] ."\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: " . $monitor['customuptimeratio'] ."%;\">".
                 "" . $monitor['customuptimeratio'] ."%".
                 "</div>".
                 "</div>";
        }
        elseif ($monitor['alltimeuptimeratio'] > 94){
            echo "<div class=\"progress\">".
                 "<div class=\"progress-bar progress-bar-warning\" role=\"progressbar\" aria-valuenow=\"" . $monitor['customuptimeratio'] ."\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: " . $monitor['customuptimeratio'] ."%;\">".
                 "" . $monitor['customuptimeratio'] ."%".
                 "</div>".
                 "</div>";
        } else {
            echo "<div class=\"progress\">".
                 "<div class=\"progress-bar progress-bar-danger\" role=\"progressbar\" aria-valuenow=\"" . $monitor['customuptimeratio'] ."\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: " . $monitor['customuptimeratio'] ."%;\">".
                 "" . $monitor['customuptimeratio'] ."%".
                 "</div>".
                 "</div>";
        }
        
        echo "</td><td>";
        if ($monitor['type'] == 4 && $monitor['port'] == "6667") {
            echo "IRC Server";
        }
        elseif ($monitor['type'] == 1) {
            echo "Website";
        } 
        elseif ($monitor['type'] == 4) {
            echo "Service";
        } else {
            echo "Other";
        }
        echo "</td>\n".
             "        </tr>\n";
    }
    echo "    </tbody>".
         "</table>";
} else {// Something went horribly wrong (like Cloudflare doing a DDoS Check... again)
    echo "    </tbody>".
         "</table>\n".
         "<div class=\"alert alert-danger\" role=\"alert\">\n".
         "    <strong>API Error</strong>".
         "Seems our uptime service provider has a small hiccup\n".
         "</div>";
}
?>