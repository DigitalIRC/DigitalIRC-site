---
layout: page
title: DigitalIRC MOTD
---
### About

**author** [MrRandom](http://twitter.com/digitalirc/)

**author email** mrrandom [at] digitalirc [dot] org

**copyright** DigitalIRC Network 2012 - 2013

**version** 1.1

motd's for all network servers are generated from the content here.
Server specific content will only be the ascii art & the server info section

### art

Contains ascii art's for the various servers that use this repo to make MOTD's

### Assembled

Contains pre-assembled files. e.g. irc rules. Also where the motd generater script output motd file(s)

### modules

Contains standard information used on each server. Staff list, webchat info, etc

### server-info

Contains server specific header's for motd's. Name, ssl ports, etc.
