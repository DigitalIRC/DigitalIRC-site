---
layout: wiki
title: Main Page
---

Welcome to the DigitalIRC Network wiki, a DigitalIRC help project which aims to provide its users with answers to their questions relating to IRC and/or the Network.

You can find us in <a href="{{site.irclink}}help">#Help</a>. Alternatively, you can <a href="{{site.webchat}}lobby">click here</a> to go to our help channel via our webchat.
<div class="row">
    <div class="col-md-4 col-sm-4 col-xs-12">
        <h3>Network Information</h3>
        <ul>
            <li><a href="{{site.baseurl}}/wiki/How_to_guide/">How to guide</a></li>
            <li><a href="{{site.baseurl}}/wiki/servers/">Servers</a></li>
            <li><a href="{{site.baseurl}}/wiki/staff/">Staff</a></li>
            <li><a href="{{site.baseurl}}/wiki/rules/">Rules</a></li>
            <li><a href="{{site.baseurl}}/wiki/oper-guide/">Oper Guide</a></li>
            <li><a href="{{site.baseurl}}/wiki/network_bans/">Network Bans</a></li>
        </ul>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
        <h3>Services Help</h3>
        <ul>
            <li><a href="{{site.baseurl}}/wiki/services/nickserv/register/">Nickname registration</a></li>
            <li><a href="{{site.baseurl}}/wiki/services/chanserv/register/">Channel Registration</a></li>
            <li><a href="{{site.baseurl}}/wiki/services/hostserv/request/">Requesting a vHost</a></li>
            <li><a href="{{site.baseurl}}/wiki/reset_your_nickserv_password/">Reset Nick Password</a></li>
        </ul>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
        <h3>Information</h3>
        <ul>
            <li><a href="{{site.baseurl}}/wiki/services/channel_management/">Channel Management</a></li>
            <li><a href="{{site.baseurl}}/wiki/services/">Services Commands</a></li>
            <li><a href="{{site.baseurl}}/wiki/ircd/modes/channel/">Channel Modes</a></li>
            <li><a href="{{site.baseurl}}/wiki/ircd/modes/user/">User Modes</a></li>
            <li><a href="{{site.baseurl}}/wiki/ircd/commands/">Server Commands</a></li>
            <li><a href="http://wiki.inspircd.org/Introduction">IRCd Documentation</a></li>
            <li><a href="{{site.baseurl}}/wiki/migrate_anope_to_atheme/">How to migrate from anope to atheme</a></li>
        </ul>
    </div>
</div>
