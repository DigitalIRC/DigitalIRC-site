---
layout: wiki
title: Channel Modes
---
<table class="table table-striped">
    <thead>
        <tr><th>Mode Letter</th>
        <th>Arguments</th>
        <th>Description</th></tr>
    <thead>
    <tbody>
<tr><td>v</td><td>[nickname]</td><td>Gives voice to [nickname], allowing them to speak while the channel is +m.</td></tr>
<tr><td>h</td><td>[nickname]</td><td>Gives halfop status to [nickname] (this mode can be disabled).</td></tr>
<tr><td>o</td><td>[nickname]</td><td>Gives op status to [nickname].</td></tr>
<tr><td>a</td><td>[nickname]</td><td>Gives protected status to [nickname], preventing them from them from being kicked (+q only, requires chanprotect module).</td></tr>
<tr><td>q</td><td>[nickname]</td><td>Gives owner status to [nickname], preventing them from being kicked (Services or only, requires chanprotect module).</td></tr>
<tr><td>b</td><td>[hostmask]</td><td>Bans [hostmask] from the channel.</td></tr>
<tr><td>e</td><td>[hostmask]</td><td>Excepts [hostmask] from bans (requires banexception module).</td></tr>
<tr><td>I</td><td>[hostmask]</td><td>Excepts [hostmask] from +i, allowing matching users to join while the channel is invite-only (requires inviteexception module).</td></tr>
<tr><td>c</td><td>No Arguments</td><td>Blocks messages containing mIRC color codes (requires blockcolor module).</td></tr>
<tr><td>f</td><td>[*][lines]:[sec]</td><td>Kicks on text flood equal to or above the specified rate. With *, the user is banned (requires messageflood module).</td></tr>
<tr><td>i</td><td>No Arguments</td><td>Makes the channel invite-only. Users can only join if an operator uses /INVITE to invite them.</td></tr>
<tr><td>j</td><td>[joins]:[sec]</td><td>Limits joins to the specified rate (requires joinflood module).</td></tr>
<tr><td>k</td><td>[key]</td><td>Set the channel key (password) to [key]. </td></tr>
<tr><td>l</td><td>[limit]</td><td>Set the maximum allowed users to [limit].</td></tr>
<tr><td>m</td><td>No Arguments</td><td>Enable moderation. Only users with +v, +h, or +o can speak.</td></tr>
<tr><td>n</td><td>No Arguments</td><td>Blocks users who are not members of the channel from messaging it.</td></tr>
<tr><td>p</td><td>No Arguments</td><td>Make channel private, hiding it in users' whoises and replacing it with * in /LIST.</td></tr>
<tr><td>r</td><td>No Arguments</td><td>Marks the channel as registered with Services (requires services account module).</td></tr>
<tr><td>s</td><td>No Arguments</td><td>Make channel secret, hiding it in users' whoises and /LIST.</td></tr>
<tr><td>t</td><td>No Arguments</td><td>Prevents users without +h or +o from changing the topic.</td></tr>
<tr><td>u</td><td>No Arguments</td><td>Makes the channel an auditorium; normal users only see themselves or themselves and the operators, while operators see all the users (requires auditorium module).</td></tr>
<tr><td>z</td><td>No Arguments</td><td>Blocks non-SSL clients from joining the channel.</td></tr>
<tr><td>A</td><td>No Arguments</td><td>Allows anyone to invite users to the channel (normally only chanops can invite, requires allowinvite module).</td></tr>
<tr><td>B</td><td>No Arguments</td><td>Blocks messages with too many capital letters, as determined by the network configuration (requires blockcaps module).</td></tr>
<tr><td>C</td><td>No Arguments</td><td>Blocks any CTCPs to the channel (requires noctcp module).</td></tr>
<tr><td>D</td><td>No Arguments</td><td>Delays join messages from users until they message the channel (requires delayjoin module).</td></tr>
<tr><td>F</td><td>[changes]:[sec]</td><td>Blocks nick changes when they equal or exceed the specified rate (requires nickflood module).</td></tr>
<tr><td>G</td><td>No Arguments</td><td>Censors messages to the channel based on the network configuration (requires censor module).</td></tr>
<tr><td>J</td><td>[seconds]</td><td>Prevents rejoin after kick for the specified number of seconds. This prevents auto-rejoin (requires kicknorejoin module).</td></tr>
<tr><td>K</td><td>No Arguments</td><td>Blocks /KNOCK on the channel. </td></tr>
<tr><td>L</td><td>[channel]</td><td>If the channel reaches its limit set by +l, redirect users to [channel] (requires redirect module).</td></tr>
<tr><td>M</td><td>No Arguments</td><td>Blocks unregistered users from speaking (requires services account module).</td></tr>
<tr><td>N</td><td>No Arguments</td><td>Prevents users on the channel from chainging nick (requires nonicks module).</td></tr>
<tr><td>O</td><td>No Arguments</td><td>Channel is IRCops only (can only be set by IRCops, requires operchans module).</td></tr>
<tr><td>P</td><td>No Arguments</td><td>Makes the channel permanent; Bans, invites, the topic, modes, and such will not be lost when it empties (can only be set by IRCops, requires permchannels module).</td></tr>
<tr><td>Q</td><td>No Arguments</td><td>Only ulined servers and their users can kick (requires nokicks module)</td></tr>
<tr><td>R</td><td>No Arguments</td><td>Blocks unregistered users from joining (requires services account module).</td></tr>
<tr><td>S</td><td>No Arguments</td><td>Strips mIRC color codes from messages to the channel (requirs stripcolor module).</td></tr>
<tr><td>T</td><td>No Arguments</td><td>Blocks /NOTICEs to the channel from users who are not at least halfop (requires nonotice module).</td></tr>
<tr><td>X</td><td>[mode]</td><td>Makes channel operators immune to the specified restrictive mode (requires exemptchanops module).</td></tr>
    </tbody>
</table>
