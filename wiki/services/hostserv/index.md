---
layout: wiki
title: Hostserv
---

### ACTIVATE

Activate the requested vHost for the given nick.
A memo informing the user will also be sent.
If an asterisk ("*") is used in place of a nick,
all waiting vHosts will be activated.

<strong>Syntax:</strong> <tt>ACTIVATE &lt;nick&gt;</tt>

<strong>Examples:</strong>

<tt>/msg HostServ ACTIVATE nenolod</tt>

<tt>/msg HostServ ACTIVATE *</tt>

### GROUP

This command will set the current nick's vhost as the
vhost for all the nicks in the group, including nicks
that will be added in the future.

<strong>Syntax:</strong> <tt>GROUP</tt>

<strong>Examples:</strong>

<tt>/msg HostServ GROUP</tt>

### LISTVHOST

LISTVHOST allows operators to see virtual hosts (also
known as spoofs or cloaks) matching a given pattern.
If no pattern is specified, it lists all existing vhosts.

<strong>Syntax:</strong> <tt>LISTVHOST [&lt;pattern&gt;]</tt>

<strong>Examples:</strong>

<tt>/msg HostServ LISTVHOST *lulz.com*</tt>

<tt>/msg HostServ LISTVHOST</tt>

### OFF

Deactivates the vhost currently assigned to the nick in use.
When you use this command, any user who performs a /whois
on your nick will see your real IP address.

For permanent removal of your vhost, please see network staff.

<strong>Syntax:</strong> <tt>OFF</tt>

<strong>Examples:</strong>

<tt>/msg HostServ OFF</tt>

### OFFER

OFFER allows you to offer a list of vhosts to the users
of your network that they can accept at will without needing
an oper to set the vhost. The string $account in the offered
vhost will be replaced by a users' account name when they TAKE it.

If the !group parameter is provided, only users with the +v flag
in that group will be able to see it in OFFERLIST and TAKE it.

<strong>Syntax:</strong> <tt>OFFER [!group] &lt;vhost&gt;</tt>

<strong>Examples:</strong>

<tt>/msg HostServ OFFER may.explode.on.impact</tt>

<tt>/msg HostServ OFFER mynetwork.users.$account</tt>

<tt>/msg HostServ OFFER !atheme-users $account.users.atheme.net</tt>

### OFFERLIST

OFFERLIST lists all vhosts currently available from
the network staff.

<strong>Syntax:</strong> <tt>OFFERLIST</tt>

<strong>Examples:</strong>

<tt>/msg HostServ OFFERLIST</tt>

### ON

Activates the vhost currently assigned to the nick in use.
When you use this command any user who performs a /whois
on you will see the vhost instead of your real IP address.

<strong>Syntax:</strong> <tt>ON</tt>

<strong>Examples:</strong>

<tt>/msg HostServ ON</tt>

### REJECT

Reject the requested vHost for the given nick.
A memo informing the user will also be sent.
If an asterisk ("*") is used in place of a nick,
all waiting vHosts will be rejected.

<strong>Syntax:</strong> <tt>REJECT &lt;nick&gt;</tt>

<strong>Examples:</strong>

<tt>/msg HostServ REJECT nenolod</tt>

<tt>/msg HostServ REJECT *</tt>

### REQUEST

REQUEST activation of the vhost. You must be <strong>identified</strong>
to a registered nick to issue this command. Please be
patient while your request is considered by network staff.

<strong>Syntax:</strong> <tt>REQUEST &lt;vhost&gt;</tt>

<strong>Examples:</strong>

<tt>/msg HostServ REQUEST may.explode.on.impact</tt>

### TAKE

TAKE allows you to set a vhost that the network staff
has offered and use it on your account. If the string
$account is in the vhost, it will be replaced with your
account name.

<strong>Syntax:</strong> <tt>TAKE &lt;vhost&gt;</tt>

<strong>Examples:</strong>
/msg HostServ TAKE $account.users.example.net
/msg HostServ TAKE example.net

### UNOFFER

Remove a offered vhost from the OFFER list.

<strong>Syntax:</strong> <tt>UNOFFER &lt;vhost&gt;</tt>

<strong>Examples:</strong>

<tt>/msg HostServ UNOFFER users.example.net</tt>

### VHOST

VHOST allows operators to set a virtual host (also
known as a spoof or cloak) on an account. This vhost
will be set on the user immediately and each time
they identify to their account. If no vhost is
specified, it will clear all existing vhosts on the account.

<strong>Syntax:</strong> <tt>VHOST &lt;nickname&gt; [&lt;vhost&gt;]</tt>

<strong>Examples:</strong>

<tt>/msg HostServ VHOST spb may.explode.on.impact</tt>

<tt>/msg HostServ VHOST nenolod</tt>

### VHOSTNICK

VHOSTNICK allows operators to set a virtual host (also
known as a spoof or cloak) on a nick. This vhost
will be set on the user immediately and each time
they identify when using said nick. If no vhost is
specified, revert to the account's vhost.

<strong>Syntax:</strong> <tt>VHOSTNICK &lt;nickname&gt; [&lt;vhost&gt;]</tt>

<strong>Examples:</strong>

<tt>/msg HostServ VHOSTNICK spb may.explode.on.impact</tt>

<tt>/msg HostServ VHOSTNICK nenolod</tt>

### WAITING

WAITING lists all vhosts currently waiting for activation.

<strong>Syntax:</strong> <tt>WAITING</tt>

<strong>Examples:</strong>

<tt>/msg HostServ WAITING</tt>
