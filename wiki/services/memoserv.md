---
layout: wiki
title: Memoserv
---

<h3>DELETE</h3>

DELETE allows you to delete memos from your
inbox. You can delete all memos with the
<b>ALL</b> parameter, all read memos with the <b>OLD</b>
parameter, or specify a memo number.

You can obtain a memo number by using the LIST
command. Once you delete a memo, the numbers of
all subsequent memos will change.

You can also SEND, READ, LIST and FORWARD memos.

<strong>Syntax:</strong> <tt>DELETE ALL|OLD|&lt;memo number&gt;</tt><br>

<strong>Examples:</strong>
<br><tt>/msg MemoServ DELETE OLD</tt>
<br><tt>/msg MemoServ DELETE 1</tt>
<h3>FORWARD</h3>

FORWARD allows you to forward a memo to another
account. Useful for a variety of reasons.

You can also SEND, DELETE, LIST or READ memos.

<strong>Syntax:</strong> <tt>FORWARD &lt;user|nick&gt; &lt;memo number&gt;</tt><br>

<strong>Examples:</strong>
<br><tt>/msg MemoServ FORWARD kog 1</tt>
<h3>IGNORE</h3>

IGNORE allows you to ignore memos from another
user. Possible reasons include inbox spamming,
annoying users, bots that have figured out how
to register etc.

You can add up to 40 users to your ignore list

<strong>Syntax:</strong> <tt>IGNORE ADD|DEL|LIST|CLEAR &lt;account&gt;</tt><br>

<strong>Examples:</strong>
<br><tt>/msg MemoServ IGNORE ADD kog</tt>
<br><tt>/msg MemoServ IGNORE DEL kog</tt>
<br><tt>/msg MemoServ IGNORE LIST</tt>
<br><tt>/msg MemoServ IGNORE CLEAR</tt>
<h3>LIST</h3>

LIST shows you your memos in your inbox,
including who sent them and when. To read a
memo, use the READ command. You can also
DELETE or FORWARD a memo.

<strong>Syntax:</strong> <tt>LIST</tt><br>

<strong>Examples:</strong>
<br><tt>/msg MemoServ LIST</tt>
<h3>READ</h3>

READ allows you to read a memo that another
user has sent you. You can either read a memo
by number or read your new memos.

<strong>Syntax:</strong> <tt>READ &lt;memo number&gt;</tt><br>
<strong>Syntax:</strong> <tt>READ NEW</tt><br>

<strong>Examples:</strong>
<br><tt>/msg MemoServ READ 1</tt>
<h3>SEND</h3>

SEND allows you to send a memo to a nickname
that is offline at the moment.  When they
come online they will be told they have messages
waiting for them and will have an opportunity
to read your memo.

Your memo cannot be more than 300 characters.

<strong>Syntax:</strong> <tt>SEND &lt;user|nick&gt; text</tt><br>

<strong>Examples:</strong>
<br><tt>/msg MemoServ SEND Kog pay your bills</tt>
<h3>SENDGROUP</h3>

SENDGROUP allows you to send a memo to all members
of a group who have the +m flag.

<strong>Syntax:</strong> <tt>SENDGROUP &lt;!group&gt; &lt;text&gt;</tt><br>

<strong>Examples:</strong>
<br><tt>/msg MemoServ SENDGROUP !opers beware of badguy</tt>
<h3>SENDOPS</h3>

SENDOPS allows you to send a memo to all ops
on a channel. Only users allowed to view the
access list can do this.

<strong>Syntax:</strong> <tt>SENDOPS &lt;#channel&gt; &lt;text&gt;</tt><br>

<strong>Examples:</strong>
<br><tt>/msg MemoServ SENDOPS #chat beware of badguy</tt>
