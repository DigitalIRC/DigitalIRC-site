---
layout: wiki
title: Botserv
---

BotServ, a bot which allows channel operators to assign bots to their channels. These bots are mostly a ChanServ representative in the channel.

### User Commands
**ACT** Has the bot perform a "/me" command.

    Syntax: /msg BotServ ACT [CHAN] [TEXT]

    Example: /msg BotServer ACT #Chat dances for everyone   
    OutpuT @Fappy dances for everyone.


**ASSIGN** Assigns a bot to your channel, assuming the bot exists in the botlist. See command(s)    **BOTLIST** and **BOT**.

    Syntax: /msg BotServ ASSIGN [CHAN] [BOTNICK]

    Example: /msg BotServ ASSIGN #Chat Fappy


**BOTLIST** Outputs a list of available bots.

    Syntax: /msg BotServ BOTLIST


**INFO** Allows you to see Bot information. (Channels, Mask, Real Name, etc.)

    Syntax: /msg BotServ INFO [CHAN/BOTNICK]

    Example: /msg BotServ INFO #Chat   
    Example: /msg BotServ INFO Fappy


**SAY** Makes the bot say something to a channel.

    Syntax: /msg BotServ SAY [CHAN] [TEXT]

    Example: /msg BotServ SAY #Chat I love pie!   
    OutpuT <@Fappy> I love pie!


##### **SET** Allows you to set various settings and features to a channel.

   **FANTASY** Allows you to enable or disable fantasy commands on a channel. Fantasy commands are like the following    !op, !deop, !kick, !ban, etc.

    Syntax: /msg BotServ SET [CHAN] FANTASY [ON/OFF]   
    Example: /msg BotServ SET #Chat FANTASY ON   
    Example: /msg BotServ SET #Chat FANTASY OFF

   **NOBOT** Makes it so a channel cannot be assigned a bot. Current bots on a channel will be unassigned when this command is performed.

    Syntax: /msg BotServ SET [CHAN] NOBOT [ON/OFF]   
    Example: /msg BotServ SET #Chat NOBOT ON   
    Example: /msg BotServ SET #Chat NOBOT OFF

   **PRIVATE** Makes it so users without Channel Admin privileges can't assign a bot to the channel.

    Syntax: /msg BotServ SET [CHAN] PRIVATE [ON/OFF]   
    Example: /msg BotServ SET #Chat PRIVATE ON   
    Example: /msg BotServ SET #Chat PRIVATE OFF


**UNASSIGN** Unassigns a bot from a channel.

   Syntax: /msg BotServ UNASSIGN [CHAN]
   Example: /msg BotServ UNASSIGN #Chat


### Oper Commands

##### **BOT** Allows opers to Add, Change, and Delete bots.

   **ADD** Creates a new bot.

    Syntax: /msg BotServ BOT ADD [BOTNICK] [USER] [HOST] [REALNAME]   
    Example: /msg BotServ BOT ADD IRC-Wiki IRC-Wiki Wiki.About.Everything.IRC IRC-Wiki

   **CHANGE** Allows you to change any information of a bot. (botnick, user, host, realname)

    Syntax: /msg BotServ BOT CHANGE [OLDNICK] [NEWNICK] [[USER [HOST [REAL]]]   
    Example: /msg BotServ BOT CHANGE Irc-Wiki IRC-Wiki   
    Example: /msg BotServ BOT CHANGE Irc-Wiki IRC-Wiki wiki.about.everything.irc IRC-Wiki

   **DEL** Deletes a bot.

    Syntax: /msg BotServ BOT DEL [BOTNICK]   
    Example: /msg BotServ BOT DEL IRC-Wiki
