---
layout: wiki
title: Request a custom bot
---

Yes, you can! There are 2 classifications of botserv bots, Public and Private. These are detailed below.

### Public Bots

Public bots are ones which anyone can use for any channel.

Anyone may request a public bot absolutely free. These bots are available for all users to assign to their channels. However, they must be completely generic and not channel specific - for example a [Spooks](http://www.bbc.co.uk/programmes/b006mf4b) themed bot called HarryPearce is acceptable, whereas a bot named GamesBot for #games would not be acceptable.

You may request a bot be added in [#help]({{site.webchat}}help) on DigitalIRC. When you ask, you must state a nickname, ident, hostname and Gecos - e.g. "HarryPearce harry@runs.MI5 Harry Pearce from Spooks". Bots must be creative and worth adding. If a staff member deems your suggestion is a good one, your bot will be added and an unused bot will be deleted.

This whole process should help us provide a better range of bots to our users.

### Private Bots

Private bots must be assigned to an oper to a channel. e.g. a bot for a site, can will only be assigned to channels related to that site.

Private bots can have any credentials that you like and will not be available to all users. When you request a private bot, you must state a nickname, ident, hostname, Gecos and channels to which the bot should be assigned. After initial creation, you may request a single change to your BotServ bot every 30 days. A single change is counted as a change to any of the bot's credentials or the set of channels in which it resides.

If you would like a public or private bot, come and see us in [#help]({{site.webchat}}help)
