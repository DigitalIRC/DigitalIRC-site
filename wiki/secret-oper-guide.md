---
layout: wiki
title: Secret Oper Guide
---

## Introduction

Congratulations and Welcome to the Team!

You have been selected by the Server Administrator of an IRC Server to be an IRC Server Operator! Because of this, it is assumed that you already have a working knowledge of most common IRC commands and concepts and a reasonable understanding of Services commands (if not, don't be afraid to ask). In all likelihood, you were selected to be an IRC Server Operator due to your dedicated assistance of users in need of help, because your attitude is helpful, friendly and you are patient, and finally, due to your interest in the success and the wish to make it the best possible IRC network.

This manual will tell you about the things you need to know to be a good IRC operator. The information given here is written as general operator guidelines on Digital IRC. IRCops must answer to their admin, and because of that, where these rules conflict with instructions you have been given by your admin, you should listen to your admin.

### IF YOU ARE A NEW IRC OPERATOR

There is a lot of information given here, and you are not expected to learn
everything at once! Also, please do refer back to this manual at a later date
if you are unsure about something, the document has been designed to be used as
a reference too.

This manual might include some instructions for things you already know how
to do. Please browse through these parts as well, since there could be new
features or details you have yet to be aware of. As an operator, you are
generally expected to have a fair understanding of the topics explained here.

Finally, keep in mind that experience is the best teacher. Everyone learns
by doing! This guide will explain a lot of things, but it's no substitute for
experience.

## TERMINOLOGY

Things you will hear about

This chapter will not explain about common IRC terms, such as bots or lag. Rather it will focus on terminology that is used mostly by IRC operators and that you might not have heard before. Some of the other terms will get a new meaning with explanations of IRC features and functionality that follows in later chapters.


### SERVICES

Services is a single program which connects in the same way as a server would to DigltalIRC. NickServ, ChanServ, MemoServ, BotServ, HelpServ and OperServ which make up Services are merely aspects of this one program. It is very important for an IRC operator to understand what IRC features are part of the IRC servers and/or the IRC network, and what are provided by Services.

The NickServ, ChanServ, MemoServ (and so on) services are not bots in the normal sense of the word. Whether they actually are bots at all or not is a debatable case. Since Services is an independent program, they may go down or crash independently of the network).

In this guide "Services" means the single program which appears as the various "services" on DigitalIRC (NickServ, ChanServ, etc.), as well as collectively referring to all of the services as a group.


### OPERS, ADMINS, NETADMIN, SERVICES ROOT, ETC.

Here's a brief explanation of the staff positions and hierarchy.

* An IRC operator (also referred to as oper, IRC op, IRCop, O:line holder, someone with a *) is someone who has been granted IRC operator status on a server. IRC operators are answerable to the admin of their server. Some operators have an O:line on more than one server, in that case they must choose a primary server. (See the definition of "lines" later in this section for an explanation about the "O:line".) The preferred term is "oper" or "IRC operator", as "IRCop" and similar titles may be read as "IRC cop".

* An admin is a person who runs (administrates) a server and is responsible for it. Admins select the operators for their server. On the IRC itself, an admin actually has almost the exact same privileges as any other oper.

Services has a number of administrative staff positions as well. These do not denote any sort of rank, except for Services access comparative purposes (see the explanations) | they signify only added responsibilities, with extra access to Services to handle these responsibilities. To be able to handle any of these responsibilities, you need to be an IRC operator. See chapter 5 for more information.

* Services Helper (SH) are normal non-oper users. They can deal with minor issues such as vhost asisgnments and can view a little more information in services. 

* Service Admins (SAs) have access to a few extra Services commands, to help deal with abusive users or other problem situations. Most notably, SAs can add and remove autokills and can reset also reset user passwords.

* Services root access users (roots) are the ones who actually manage and run Services. They have access to several Services features not available to anyone else, in addition to all SA features.

### GLOBALS, GLOBOPS, WALLOPS, CHATOPS AND LOCOPS

A global (short from "global notice") is a notice sent by an operator to all DigitalIRC users who are using the network. Globals can also technically be done with normal messages instead of notices, but in practice that is never done for a number of reasons.

Notices may also be sent to a limited group of users, either based on which servers they are using, or what their address is. If the notice is sent to the clients on only one server, it's usually called a server notice (confusingly enough, since server notices are also messages sent by servers to anyone who has the usermode +s set | to avoid this these kind of globals could be labelled after the server's name, for example a "davis notice"). If the notice is sent to part of a network, for example "*.eu.dal.net" (all the users on the European servers), it is a network part notice. If it's sent to users based on their address/hostname, it's called a domain notice or notice to hostmask.

A globop is a message sent by an operator or a server to all IRC operators who have set the usermode +g. Globops are a means for opers to discuss important network issues without users seeing. For extended discussions, joining a channel is strongly recommended. Some people might refer to globops as "globals" too, but this is not proper usage and is discouraged. Globops should not be used for idle chat, see chatops (below) for that.

A wallop is a message sent by an operator to all users who have the usermode +w set (This is set on all users connect to DigitalIRC). Only IRCops can send them, but they are visible to anyone who sets the usermode. Wallops are a bit "antique", and because of this they shouldn't be used much. The point is arguable though. Many curious users set themselves +w in order to see interesting operator messages. The best current use for them is for announcing network issues to the "interested users", especially in cases when a global is not proper.

### REROUTING


Rerouting means changing the current network layout: the order of how servers connect to each other. The reason for rerouting is to get rid of a bad network connection and start using one that works better. Reroutes are basically done by issuing a /squit (or /rsquit) command to disconnect a link on the network, and then using a /connect (or /rconnect) to establish another link elsewhere. There are actually many things to consider in rerouting | for more information, see the sections explaining /connect, /squit and especially chapter 7 which explains rerouting.

### SERVER LINES

IRC servers all have a configuration file which controls various aspects of the server, usually called ircd.conf. This file consists of various "lines", which are all named after the first character in that line. Thus there are O:lines, K:lines, C/N:lines, etc. Some of these lines can be viewed with the /stats command on IRC.

Here's a brief explanation of what each line does:

Line Type | Description
--------- | -----------
A:line | Specifies what information is seen with the /admin command.
C:lines | Specify which servers the server can connect to.
D:lines | Connect rules for all connects.
d:lines | Connect rules for autoconnects only.
I:lines | Define which clients are authorized to connect to the server, and what connection class they should use (see Y:lines).
K:lines | Define which clients are prohibited access to the server (banned).
k:lines | Define which clients are prohibited access to the server temporarily (settable by server opers).
M:line | Specifies the server name, description and the port number it should "listen" to.
N:lines | Specify which servers are allowed to connect to the server.
O:lines | Define which clients can access operator commands on the server.
o:lines | OBSOLETE, however the term "local o" is still in use. Define which clients can access local operator commands (restricted set of operator commands; eg. /kill will only work on users on the same server).
P:lines | Set which additional ports the server may listen to. (Also select IP addresses to use if the computer has more than one.)
Q:lines | Set which nicknames cannot be used on that server.
q:lines | Set which server names are not allowed on the network (need to be set on all servers for things to work properly).
U:lines | Define which servers are authorized to make changes to user and channel mode settings regardless of actual user status (restricted to services.int on DigitalIRC).
Y:lines | Specify how the server accepts and treats connections by setting up "connection classes".
Z:lines | Define which hosts (based on IP address) are not allowed to connect to the server at all.
z:lines | Define which hosts (based on IP address) are not allowed to connect to the server at all (settable by opers).

### KILLS, KILLABLE OFFENSES

A kill is an IRC term for disconnecting a user from the network. Care should be taken so that nobody confuses this with real world killing. See the operator command /kill for more information.

Killable offences are those breaches of DigitalIRC rules for which an operator may use the /kill command to disconnect the user.


### K:LINES, AUTOKILLS (AKILLS)

A K:line is a ban for a user or group of users on an IRC server. K:lining means adding a K:line, or in general terms banning a user from a server or the entire network. Permanent K:lines are added to the server's configuration file by the server's admin. Temporary k:lines (a lower case k is used for them) may be added by that server's operators on IRC, and it stays until the server goes down or a rehash is done.

Autokill (commonly shortened to akill) is a Services feature to automatically kill any user whose address matches a mask in the autokill list. Additionally, Services adds a temporary k:line on the server the user was using. Autokills can be set and removed by Service Admins, while the autokill list is viewable by all opers. Autokills are the key feature in keeping abusive people out of DigitalIRC.


### SERVICES IGNORES

Services ignore works just like a normal ignore: anyone in Services' ignore list gets ignored by Services (autokills, akick's and similar things are still enforced, though). Ignores can be set or removed by Service Admins (and above). 
Ignores are a less forceful way (than autokills) to deal with abusive people, and they are mostly used in cases of Services abuse.


### MARKS

Nicknames and channels registered with Services can be marked by an Operator. The purpose of a mark is to show that the nick/channel has been looked over by another and nobody else should touch it without talking to the person who placed the mark. It can be used in situations when a nick/channel ownership issue is complex or uncertain.


### CLONEBOTS (CLONES, CLONING)

Clonebots (commonly shortened to clones) are a form of bots used to flood other people. Since IRC prevents you from sending more than one message every two seconds, in order to effectively flood someone you need to use more than one client. 
Clonebots are mostly quite easy to tell apart from other users because their nickname, username and ircnames are usually formed of random characters. There is no valid reason for having clonebots on DigitalIRC, which is why they have been banned.

Running clonebots is sometimes called cloning. Please note that having a second connection to the network is not the same as running clonebots!

### IRCD

ircd is the name of the IRC server program (it comes from "Internet Relay Chat Daemon", but spelled all lowercase according to unix tradition). It is used when talking of the actual server program, while "server" usually refers to the whole entity that a server running on DigitalIRC is: the admin, server operators, link permission, etc.

### LINKS

A link is a connection between two servers. That's why the command to show all the server connections on the network is called /links.

Another meaning for a link is as "permission to be linked to the network", as in all the DigitalIRC servers have a link (to DigitalIRC) while those servers which are applying don't have it (yet). A "delink" is the opposite of this, it means "not part of the network".

### SYNC(H)S

Synch (or sync) is short from "synchronization". On IRC the term is used to describe the act of exchanging data between two servers in order to synchronize the databases, so that both of the servers hold the same information about what users are online and which channels have been formed (known as "synching").
It's also used when describing the status of two connected servers which have finished synching and when they are working together seamlessly (known as "synched").

A resynch is when two servers connect to each other and synch after a netsplit.

A desynch occurs when the servers on a network do not agree about the state of some nickname or channel. For example, a single desynched server might hold that a user has ops, while all the other servers see him/her as just a normal channel member. This means that the desynched user is able to kick and ban others seemingly without ops on the channel, for example.


### JUPES

A jupe (comes from "jupiter", a user's nick once of EFnet) is a fake connection that stops something else from connecting on IRC by taking their name/place. On DigitalIRC, it usually means a fake server using another server's name, stopping the real server from connecting to the network. 
Jupes are used very rarely, and only as an emergency and temporary measure.

### SENDQ, RECVQ (RCVEQ)

The 'Q' in these comes from "queue". The sendQ is used by the server to buffer the outgoing (send) data, while the recvQ (receive queue, also sometimes written as rcveQ) stores incoming (receive) data from a client or server.
There is a separate sendQ and recvQ for each client and server connection. Also, for server connections, the sendQ sizes may be different on each server, eg. server A's sendQ for the connection to server B may be different from the sendQ that server B has for its connection to server A.

For clients, exceeding the receive queue is a possible sign of flooding, while exceeding the send queue is likely due to using a command which generated a lot of output (such as /list).

For servers, exceeding the sendQ is a common symptom when a connection is broken, but the server doesn't receive an actual notification of the fact. The server will keep trying to send data through the link to the other server, until the sendQ overflows, and the connection is closed.

## Operator Commands

So that you don't have to look through stuff you don't need. All I'll say is ''/helpop'' is a excelent reference for both command descrions and example uses. Also there are a list of services commands available via the [DigitalIRC wiki]({{site.baseurl}}/wiki/).

## Oper Duties

1. Help maintain the network and your server
* Help the users
* Enforce DigitalIRC rules
* Behave in a fitting manner for a DigitalIRC IRC operator
* Keep up to date on DigitalIRC matters, rules and policies
* Do not share confidential information with non-opers

### HELP MAINTAIN THE NETWORK AND YOUR SERVER

You should monitor the state of the network and your server. Mostly this means watching out for netsplits and lag, reconnecting and rerouting servers if necessary.
You should also look into anything that seems strange or just "wrong" on the network or your server. Your admin may give instructions to you of how your should help with running the server.

### HELP THE USERS

All opers should help users. If a user asks for help, and you can't help him/her yourself for some reason (for example, don't know the answer, you are busy with something else, or you are "off duty"), you should try to locate someone else who can help the user. At the very least you should politely indicate that you cannot help and try to direct the user to another likely source of help.

### ENFORCE DigitalIRC RULES

Opers should uphold DigitalIRC's rules, as outlined in the DigitalIRC Policies and Procedures document. Use the means available to you to deal with any violation of rules in an appropriate manner. The guidelines given in this document, by your trainer or admin, and in other DigitalIRC sources should help you in choosing the right approach to each situation.

### BEHAVE IN A FITTING MANNER FOR A DigitalIRC IRC OPERATOR

You should behave as is appropriate for a DigitalIRC IRC operator. This is especially true in times when you are "on duty" -- an acting representative of DigitalIRC in some way. For example, when you are helping a user, or dealing with some breach of DigitalIRC rules, or writing mail to a DigitalIRC mailing list.

### KEEP UP TO DATE ON DigitalIRC MATTERS, RULES AND POLICIES

DigitalIRC is a growing and evolving community. Things are constantly changing. There might be a new policy added, new features for IRC or Services added, administrative changes taking place, or just about anything else... Because of this it is very important that you keep up with DigitalIRC news.


### DO NOT SHARE CONFIDENTIAL INFORMATION WITH NON-OPERS

As a DigitalIRC operator, you will be informed of many confidential issues, facts and matters. Whether these are related to DigitalIRC's security, or to the network's users (eg. nickname/channel passwords) it is VERY important that you do not disclose these to anyone who is not a DigitalIRC operator. This especially concerns information obtained through the privileged operator sources (the ops mailing list, globops, and so on).

Severe penalties can and have been handed down for violations of this rule.

### "ON DUTY" AND "OFF DUTY"

It is recognized that operators may be "on duty" and "off duty". Naturally you have a greater freedom to act as you choose when you are off duty, but you should keep in mind that the activities of an oper reflect upon DigitalIRC even when the operator is not acting as a representatives of it. When you are on duty, you should always pay special attention to conducting in a professional manner.

When are you on duty, and when off duty? The single definite rule is that when you are acting as DigitalIRC's representative or a DigitalIRC staff member, you are on duty. It is actually possible to be on duty and off duty at the same time, for example you can be "on duty" on a help channel and "off duty" in a private channel. In this case, anything you do or say on the help channel should be considered being on duty, while the private channel activity is clearly off duty.

The interpretation depends upon each situation. If you doubt, it's better to err on the safe side and behave as if you are on duty.

Here's a few indications that you are on duty:

* Usermode +o set, using operator commands
* Usermode +h set, or helping users on a recommended help channel, or helping as a member of DigitalIRC staff (for example, when a user requests help in a private message)
* Having channel ops when doing so as a representative of DigitalIRC staff (for example, ops on #Help or another IRC operator channel, or opped on a channel using OperServ)
* Participating in the mediation of a user dispute
* Monitoring user or channel situations, incognito or not
* Attending DigitalIRC staff online meetings
* Posting email on DigitalIRC mailing lists


## GENERAL CONDUCT GUIDELINES

Here are a few guidelines for general conduct. The first two should not need to be included, but nevertheless they are listed here so that nobody can say that they weren't clearly stated.

* Always follow DigitalIRC's rules!
* Do not abuse your operator privileges.
* Respect and be considerate to all other users. This includes both regular users and other operators.

The following should be observed when on duty:

* Avoid swearing and derogatory comments.
* Do not exhibit sexual behaviour in public.
* Keep calm -- if you become upset or angry and cannot remain objective, it is a good idea to ask a fellow operator to handle the situation if possible.

### STAFF MISCONDUCT

The procedure for registering a complaint about a DigitalIRC staff member is:

1 Try to resolve the problem by talking to the other person directly. 
* If there is still a problem, report it to the person's superior. If the person is an IRC operator and it's a generic complaint, report it to that person's admin. If you do not know the oper's primary server, report it to the admin of the server that the operator was opered on at the time (it is likely you will be directed to talk to the responsible admin).
* If the problem remains unresolved after the superior has investigated and made a decision, a report can be sent to the Network Admin (MrRandom) with supporting documentation (including all the logs from all the meetings with the person and with the superior, if available).
* If it is escalated to the Network Admin level then their decision is final and absolute.

The possible consequences of misbehaviour are decided by the superior of that staff member (either the IRC oper's admin or the team leader, as appropriate) alone or in conjunction with the other staff. Consequences will be proportional and relevant to the offence and may include:

* Demotion to a local operator for a probationary period
* Personal and public apology
* Replacing or reconstructing damaged records
* Removal of channel AOPs; temporarily or permanently for repeated misuse
* Removal of the O:line or staff position
* Removal of Services Admin or CSop access
* Refusal of future O:line opportunities
