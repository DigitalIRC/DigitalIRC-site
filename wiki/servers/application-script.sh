#!/bin/bash

serverList=(binary mooo shire)

echo Start Script > DigitalIRC-Traceroute
echo ------------ > DigitalIRC-Traceroute

for var in "${serverList[@]}"
do
    echo Traceroute: $var > DigitalIRC-Traceroute
    traceroute -4 $var.digitalirc.org > DigitalIRC-Traceroute
    echo ============ > DigitalIRC-Traceroute
    echo ping: $var > DigitalIRC-Traceroute
    ping ping -s 1024 -c 100 -q $var.digitalirc.org > DigitalIRC-Traceroute
    echo ------------ > DigitalIRC-Traceroute
done