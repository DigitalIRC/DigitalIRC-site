---
layout: wiki
title: Server Linkage Application
---

Server Application For Digital IRC Network.

When complete please fill out a [support ticket](http://tickets.digitalirc.org/open.php) with the help topic "General Inquiry" and the issue summary of "Server Application", with a copy of the filled out form bellow in the issue details section.

### INSTRUCTIONS:

In order to process your application, you must complete each of the following four sections. When complete please attach to a support ticket 

<ol>
    <li>Basic server information
        <ol>
            <li>Server's geographic location (city,state/country):</li>
            <li>Connection (speed) to the Internet:</li>
            <li>Backbone(s) connected to:</li>
            <li>Fully qualified hostname: (If already assigned one)</li>
            <li>IP address:</li>
        </ol>
    </li>
    <li>Detailed server information
        <ol>
            <li>Operating system/version:</li>
            <li>Number of file descriptors the ircd user is can use:</li>
            <li>Processor/speed:</li>
            <li>Memory:</li>
            <li>Is this a virtualized server:</li>
        </ol>
    </li>
    <li>Optional Information
        <ol>
            <li>Other tasks run on this machine:</li>
            <li>Intended server name:</li>
            <li>Intended listening ports:</li>
            <li>Number of clients you will allow:</li>
        </ol>
    </li>
    <li>Admin Info.
        <ol>
            <li>Admin's Nickname:</li>
            <li>Admin's E-Mail address:</li>
            <li>Does Admin have access to the root account:</li>
        </ol>
    </li>
    <li>Server host details
        <ol>
            <li>Server owner's name (or ISP):</li>
            <li>Server owner's phone number:</li>
            <li>Server owner's E-Mail address:</li>
        </ol>
    </li>
</ol>

### Section 6: Free Answer (Use as much space as needed).

    1. How long have you been involved with DigitalIRC? How long have you been on IRC in general?


    2. Why do you feel your server should become part of DigitalIRC?


    3. Please list any previous IRC Network staff experience you may have. Please include the network(s) and server(s) that you had this experience on.


    4. What IRCops do you have in mind? Please tell us a little about them and make sure to include any past IRCop experience they may have had as well as why you chose them.


    5. How much active time would you estimate you will be able to dedicate to this network per week? This means actually paying attention to IRC; idling does not count.


    6. What kind of duties and obligations do you feel you should have as a Server Admin, in regards to other network staff and the userbase as a whole?


    7. Do you have anything you would like to add that you haven't yet been able to tell us? If so, please describe.


### Section 7: Supplementary Files

Please attach to this application the output from the [server application script]({{site.baseurl}}/wiki/servers/application-script.sh). This includes traceroutes to various DigitalIRC servers along with other relevent data.
The script may be done at any time of day.

