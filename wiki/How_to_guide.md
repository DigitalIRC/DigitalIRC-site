---
layout: wiki
title: Getting Started
---

Hello and welcome. If you are new to the world of IRC, hopefully this (rather large) tutorial will give you a good grounding in the concepts. It won't make you a pro, but it will give you enough information to kickstart learning more. If you really want to there is a looot of things to learn about IRC.

### So what is IRC?

IRC stands for Internet Relay Chat. It is basically a simple chat program. The idea is that you first connect to a IRC server and then you can join rooms (called channels), or privately message people to your hearts content.

### Getting started

First you will need an irc client. There are lots of them out there, and I will cover a few of them later. Assuming you have already chosen a client and you have it open in front of you, the server name is binary.digitalirc.org. An easy way to quickly get started in most clients is to simply type /server irc.digitalirc.com and hit enter in the main window (there is usually a line for entering text towards the bottom).

A whole bunch of text will normally spew out in front of you, it should start

<blockquote>-!- Welcome to the DigitalIRC IRC Network YourName<br>
-!- Your host is shire.digitalirc.org, running version InspIRCd-2.0<br>
-!- This server was created Fri Nov 23 2007 at 19:54:54 SGT</blockquote>

YourName is usually selected during the install procedure, but can vary from client to client. If you aren't happy with whatever is here you may change that at any time with the command /nick NewName where NewName is whatever you want your screen name to be.

The final few lines of text should read

<blockquote>-!- - Welcome to the DigitalIRC Network!<br>
-!- End of /MOTD command.</blockquote>

And you are left sitting there, with a command prompt patiently waiting your next move. This is the point, in my experience, that people start to get a little nervous about this strange and scary process - but it's almost done, so stick with it a little longer...

OK, so the next thing to do is to join a channel and see what's going on there. However, if you are new to this whole thing I recommend an additional step. Type /list and hit enter. This command lists all of the channels on the server:

<blockquote>-!- #boaf 3 [+ntr] BAF LIVES FOREVER<br>
-!- #cnbl 3 [+ntr] yummmm nachoz<br>
-!- #halo 1 <br>
-!- #kick 1 <br>
-!- #gmod 2 [+ntr] LOLOLOL. || models: http://games.slainvet.net/gmod.zip || steam://connect/games.slainvet.net:27015<br>
-!- #olda 2 [+ntr] ;Welcome to the OLDA IRC channel, where people who are Over Legal Drinking Age meet  | Register <br>
your nickname, so you can get op status automatically |OLDA OFFICIAL FORUM FIXED. ENJOY!<br>
...<br>
-!- #TheZoo 2 [+nt] ola espaniola
-!- #gururpg 2 [+ntr] Welcome to idlerpg, to register type /msg IRPG-Guru REGISTER <char name> <password> <char <br>
class> || http://idlerpg.net/ for more commands and info.<br>
-!- End of /LIST</blockquote>

the server names are the things with a '#' symbol in front, the number that follows is the number of people in that room. After the number you might see square brackets '[]' surrounding some seemingly random letters. These represent the channel modes, don't worry too much about that just yet. Finally, there is the topic which can be set by people with special privileges in that room. For example, one of the channels is called '#TheZoo', when I copy/pasted that line there were two users in the room and the topic is 'ola espaniola'.

If you see a channel you want to join, the command is very simple, /join #channel, the channel with the most users is most likely going to be #TheZoo, so you could type, /join #TheZoo

That should take you into the channel, where you will hopefully see people chatting away. Depending on your client, you should see a list of usernames representing everyone who is also in this channel. For most clients this is in list form to the left or right of the main chat window.

Chatting is simple, just type what you want to say in the command line and hit enter. Remember if you start with a '/' the server will try to interpret it as a command and your text will not be displayed in the chat window.

### Useful commands

We have covered some, but this might be helpful as a quick reference:

**/server** - your client will connect to a server. Format: /server irc.digitalirc.com

**/connect** - identical to /server.

**/join** - if you follow this command with a channel name (don't forget to include the '#') you will join that channel. If the channel name does not exist, it will be created and you will be moderator (or 'op') of that channel. Format: /join #DigitalIRC

**/leave** - leaves the channel. Format: /leave #TheZoo you don't have specify a channel, but if you only want to leave one specific channel when you are in multiple channels you should include it.

**/part** - same as leave for those of a more sophisticated bent.

**/partall** - leaves all channels.

**/quit** - quits from the IRC server. You can also add a message, meant to be used to specify why you quit, but more often than not it is used to get in the last word in an argument before slamming the door. Format: /quit I have better things to do than this. will display YourNick has left the room (quit: I have better things to do than this).

**/away** - sets your status to away or afk. You will need to set a message which will be displayed if anyone tries to privately message you or if they use the /whois command. Format: /away Making a cup of coffee

**/me** - Makes you appear to do an action. Format: * CMon is about to become a black belt will display * YourName is about to be become a black belt

**/msg** - allows you to send a private message to another user. Format: /msg TheirNick hello, how's it going? where TheirNick is the screen name of the person you wish to pm.

**/privmsg** - Identical to /msg, sometimes only one or the other works.

**/whois** - displays information about a user. /whois TheirNick, would display

<blockquote>[TheirNick] (TheirIdent@ThereNick.user.digitalirc.org): TheirName<br>
[MrRandom] some.digitalirc.org :One of the digitalIRC network servers<br>
[MrRandom] is an IRC operator<br>
[MrRandom] is available for help.<br>
[MrRandom] is logged in as TheirNick<br>
[MrRandom] is using a secure connection<br>
[MrRandom] End of WHOIS list</blockquote>

**/helpop** - usually displays a list of common commands, if you type /help list it might give information about the list command.

### Other commands to be aware of

**/ignore** - stops a certain user's messages from being displayed in the chat window. Format: /ignore TheirNick

**/ison** - tells you if certain people are online. Format: /ison Nick1 Nick2 Nick3. If any of the users are online their Nicks will be echoed back to you. For example, if Nick1 and Nick3 are connected to the irc network:

<blockquote>/ison Nick1 Nick2 Nick3<br>
Nick1<br>
Nick3</blockquote>

**/motd** - displays the server's message of the day. Nothing to get excited about, It has read "- Welcome to the DigitalIRC Network!" and most clients display the "Message of the Day" upon connection.

**/time** - returns the server time.

**/whowas** - works the same as whois, but for use when someone has left the server. 

**/kick** - Forces a user from the channel (only ops can do this). Format: /kick TheirNick.

### Etiquette

Most people know how to be polite, all the same rules of ettiquette within other online communications apply in IRC. Different channels can have quite different standards of manners, but if you aim to be more polite than the average user you shouldn't go too far wrong. Here are some guidelines:

* Be nice - nobody likes an ass.
* Don't spam - avoid copy/pasting large text documents into the text window, try not to post more than 4 lines of anything in rapid succession.
* Be easy to read - in busy channels the text can scroll pretty quickly. Don't make it any harder on people to try and read what you say. Avoid l33t speak and general stupid alterna-spellings as much as possible. "W455uP p33pz!!!!!!! I haz c0RRuptard replehz!!! hoW d0 aye fyxes it!!!!" is not good, it's not clever, and it won't make you any new friends.
* Don't spam. Really. Don't do it. In IRC terminology it is called flooding and is grounds for getting banned.
* Try to set away messages if you are going away for a significant period of time. If someone is trying to get hold of you, at least they have some idea why you aren't responding.
* Try to avoid frequent nick changes. When you change your nick it tells everyone in the channel. If you are doing a lot of nick changes, you can flood the channel with lots of near useless information.
* Keep funky colours to a minimum too. You can change the displayed colour of your text which has its uses - but constantly doing it can be kind of annoying to everyone who have to suffer reading it.
* Don't SHOUT! Capitals have their place, and the occasional all-CAPS word is fine for emphasis here and there. However, please don't over do it.
* Say goodbye when you are leaving. Not always possible, but if it is and you are in a conversation with someone, please do so.
* Smileys are intended to denote emotion to make up for the fact that the written word can be misconstrued since there is no body language or tone. You don't need nine smileys to express that something pleases you or that you were being sarcastic.

Also, read the [RULES]({{site.baseurl}}/wiki/rules/) to get a feel of what is tolerated and what is not tolerated.

### Terms you should know in IRC

**op**: Ops, or Channel Operators, are the channel's mods. They have considerable power within a room including the ability to ban users who are misbehaving. Be especially polite to channel operators. Most clients will display ops and other dignitaries in some fashion to make them obvious. There maybe a symbol next to their name (eg: @) or a coloured button next to it instead.

**Half-op**: Basically the same as an op.

**IRC operator/Oper**: If ops are the moderators, then IRCops are the supermods. They control the whole server (or network, see below).

**IRC admin/Admin**: Essentially they are the gods of the server.

**Network Admin**: Essentially they are gods of the network. Do not piss them off.

**channel**: Channels are like other chat 'rooms'. They are a virtual rooms which you can join and chat to some of the other users on a server. Channels usually have a set topic or purpose.

**flood**: Sending many lines of text in a short period of time. Don't do it.

**IRC bot**: A bot is just a client running a script that interacts with the the users in various ways (anything from updating them with news headlines, channel statistics, speaking with a limited AI etc) NickServ is a bot, as is ChanServ.

**IRC Server**: A computer that is running IRC server software that allows users to connect and create channels to chat in.

**IRC network**: Sometimes multiple servers are joined together in a network to help distribute the load. DigitalIRC has another server with the name shire.digitalirc.org: connecting to this server will allow you to join all the same channels and chat with the same people as if you had connected to binary.digitalirc.org.

**netsplit**: sometimes the servers in an IRC network stop working in harmony. The technical details aren't important. Basically if you are in binary.digitalirc.org and there is a netsplit, you stop being able to communicate with users on shire.digitalirc.org and vice-versa. They are easily identified when half the room vanishes at once.

**kick**: Someone with certain powers (eg., ops) can kick a user out of a room. They can still rejoin, though doing so straight-away annoys some ops - so be careful.

**kill**: Usually reserved for higher-ups such as IRCops, this will disconnect a user from the whole server.

**ban**: Pretty self-explanatory. A banned user can either not rejoin the channel until the ban is lifted. Ban evasion does not impress the powers that be, so don't bother trying.

**k-line**: k-line is used to ban a user from a server, permanently or just temporarily. If you are k-lined from binary.digitalirc.org you can still join shire.digitalirc.org. The k means kill. 

**g-line**: g-line is a special form of k-line. The g stands for global and it means that the user will be banned from all servers on the network.

**NickServ**: A bot who selflessly looks after the administration of nicknames.

**ChanServ**: Another administration bot that looks after channels. ChanServ can be set to auto-op certain users (usually requires having registered a nick with NickServ and successfully identifying yourself), and keep channels open (visible in the /list) even when there are no users in them.

**Channel 0**: For historical reasons, joining Channel 0 (no '#' sign) will disconnect you from all channels (but not the server). In the olden days (80s and early 90s) channels didn't have names - just numbers.
